var RegEx = /[A-Z 0-9]/;

$(function() {
	$('.container-short').css("min-height", $(window).height() - 60);
});

$(document).ready(function(){
	$('#txtContent').keyup(function(e){
		var $th = $(this);
		$th.val( $th.val().replace(/[^a-zA-Z0-9\s]/g, '' ) );
	});
});
