
'use strict'; // sử dụng strict mode của javascript

const
	config = require('config'),
	db = require('./model/db'),
	User = require('./model/authentication'),
	crypto = require('crypto'),
	fb_user = require('./model/fb_user'),
	express = require('express'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	expressValidator = require('express-validator'),
	flash = require('connect-flash'),
	exphbs = require('express-handlebars'),
	session = require('express-session'),
	Passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	mongoose = require('mongoose'),
	bcrypt = require('bcryptjs')

const APP_SECRET = config.get('appSecret');

var app = express();

app.use(bodyParser.json({ verify: verifyRequestSignature }));
app.use(express.static('./public'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.set('view engine', 'ejs');
app.set('views','./views');
app.set('trust proxy', 1);

app.use(session({
	secret: 'mysecret',
	cookie: {
		maxAge: 1000*60*60
	}
}));

app.use(Passport.initialize());
app.use(Passport.session());
//Express Validator
app.use(expressValidator({
	errorFormatter: (param, msg, value) => {
		var namespace = param.split('.'),
		root = namespace.shift(),
		formParam = root;

		while(namespace.length) {
			formParam += '[' + namespace.shift() + ']';
		}
		return {
			param : formParam,
			msg   : msg,
			value : value
		};
	}
}));

app.use(flash());
//Global Vars
app.use((req, res, next) => {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	res.locals.user = req.user || null;
	next();
});

function ensureAuthenticated (req, res, next) {
	if(req.isAuthenticated()) {
		return next();
	} else {
		res.redirect('/');
	};
};

var	send = require('./routes/about'),
	changePassword = require('./routes/changePassword')
	
app.use(express.static(__dirname + '/public'));
app.use('/send', send);
app.use('/doi-mat-khau', changePassword);

app.route('/')
	.get((req, res) => { 
		res.render('login')
	})
	.post(Passport.authenticate('local', {
		failureRedirect: '/',
		successRedirect: '/send',
		failureFlash: true}), (req, res) => {
			res.redirect('/');
	});

app.get('/logout', (req, res) => {
	req.logout();
	req.flash('success_msg', 'You are logged out');
	res.redirect('/');
});

var countLogIn = 0;

Passport.use(new LocalStrategy (
	(username_, password, done) => {
		mongoose.model('accountlist').findOne({username: username_}, function(err_, userDoc){
			if (err_) {
				return done(err_);
			}
			if (userDoc){
				bcrypt.compare(password, userDoc.password, function(err, isMatch){
					if(err) throw err;
					if (isMatch){
						countLogIn = 0;
						
						return done(null, userDoc);
					} else {
						countLogIn++;
						console.log("So lan dang nhap sai: ", countLogIn);
						return done(null, false);
					}
				});
			} else {
				countLogIn++;
				console.log("So lan dang nhap sai: ", countLogIn);
				return done(null, false);
			}
		});
	}
));

Passport.serializeUser((user, done) => {
	done(null, user.id);
});

Passport.deserializeUser((name, done) => {
	User.getUserById(name, (err, user) => {
		done(err, user);
	});
});

function verifyRequestSignature(req, res, buf) {
	var signature = req.headers["x-hub-signature"];

	if (!signature) {
		console.error("Couldn't validate the signature.");
	} else {
		var elements = signature.split('=');
		var method = elements[0];
		var signatureHash = elements[1];

		var expectedHash = crypto.createHmac('sha1', APP_SECRET)
							.update(buf)
							.digest('hex');

		if (signatureHash != expectedHash) {
			throw new Error("Couldn't validate the request signature.");
		}
	}
}

app.set('port', process.env.PORT || 3003);
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
module.exports = app;

