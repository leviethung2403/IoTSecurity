

const
	mongoose = require('mongoose')

module.exports = {
	updateNewPassword: function(schoolId, newPass) {
		var query = {username: schoolId};
		var update = {password: newPass };
		var option = {new : false};
		console.log("username: ", schoolId);
		console.log("password: ", newPass);
		mongoose.model('accountlist').findOneAndUpdate(query, update, option, function (err, userDoc){
			if (err)
				console.log('Error when save data.');
			else {
				console.log("Mật khẩu mới đã được lưu");
			}
		});
	}
};