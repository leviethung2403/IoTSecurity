

var express = require('express'),
	router = express.Router(),
	aesjs = require('aes-js')

var host = "100.100.11.229";

router.get('/', (req, res) => {
	if (req.isAuthenticated())
		res.render('about', {page: 'contact', logged : true, schoolName: "Admin", alert_danger: '', alert_success: ''});
	else 
		res.redirect('/');
});

router.post('/', (req, res) => {
	if (req.isAuthenticated()) {
		var content = req.body.txtContent.toUpperCase(),
			topic = 'topic/test123';

		let option = {
			port	: 1883,
			host	: 'mqtt://127.0.0.1',
			cliendId: 'NodejsServer'+Math.random().toString(16).substr(2,8),
			username: 'vihu',
			password: '1',
			keepalive: 60,
			reconnectPeriod: 1000,
			protocolId: 'MQIsdp',
			protocolVersion: 3,
			clean: true,
			encoding: 'utf8'
		}


		if (content.length >= 25){
			res.render('about', {page: 'contact', logged : true, schoolName: "Admin", alert_danger: "Độ dài chuỗi phải ngắn hơn 25 ký tự.", alert_success: ''});
		} else {
			var mqtt = require('mqtt')
			var client  = mqtt.connect("mqtt://127.0.0.1", option);

			client.on('connect', function () {
				console.log(Buffer.from(content).toString('base64'));
				content = Buffer.from(content).toString('base64')
				var st = "";
				for (var i=(content.length-1); i>0; i--) {
					st += content[i-1];
				}
				console.log("st: ", st);
				client.publish(topic, st);
			})
			 
			client.on('message', function (topic, message) {
				console.log(message.toString())
				client.end()
			})

			client.on('error', function(err){
				console.log(err);
			})
			res.render('about', {page: 'contact', logged : true, schoolName: "Admin", alert_danger: '', alert_success: "Gửi thành công!"})
		}
	} else {
		res.redirect('/');
	}
});

module.exports = router;
