

var express = require('express')
var router = express.Router()
var bcrypt = require('bcryptjs')
var funcSaveNewPass = require('../api/updateNewPass')

router.get('/', (req, res) => {
	if (req.isAuthenticated()) {
		res.render('change_pass');
	} else {
		res.redirect('/');
	}
});

router.post('/', (req, res) => {
	var 
		current_pass=req.body.current_pass,
		new_password=req.body.new_password,
		reType_password=req.body.reType_password,
		password_in_database=req.user.password
	bcrypt.compare(current_pass, password_in_database, function(err, isMatch){
		if(err) throw err;
		if (isMatch){
			if ((new_password === "") || (reType_password === ""))
				res.redirect('/doi-mat-khau'); //2 Mật khẩu nhập lại trống
			else if ((new_password != reType_password))
				res.redirect('/doi-mat-khau'); //Mật khẩu mới không trùng nhau.
			else if ((new_password === current_pass)){
				res.redirect('/doi-mat-khau'); //Mật khẩu mới giống mật khẩu cũ.
			}
			else { //Hash mật khẩu vào update database.
				bcrypt.hash(new_password, 10, function(err, hash) {
					funcSaveNewPass.updateNewPassword(req.user.username,hash);
					res.redirect('/');
				});
			}
		}
		else {
			res.redirect('/doi-mat-khau'); //Mật khẩu hiện tại không khớp.
		}
	});
});

module.exports = router;
