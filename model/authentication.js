
const 
	mongoose = require('mongoose'),
	bcrypt = require('bcryptjs')

module.exports = {
	createUser: function(newUser, callback) {
		bcrypt.genSalt(10, function(err, salt) {
			bcrypt.hash(newUser.password, salt, function(err, hash) {
				newUser.password = hash;
				newUser.save(callback);
			});
		});
	},
	getUserByUsername: function(username, password_, isMatch_) {
		mongoose.model('accountlist').findOne({_id: username}, function(err_, userDoc){
			if (err_) throw err_;
			if (userDoc){
				bcrypt.compare(password_, userDoc.password, function(err, isMatch){
					if(err) throw err;
				});
			};
		});
	},
	getUserById: function(id, callback) {
		mongoose.model('accountlist').findById(id, callback);
	}
};