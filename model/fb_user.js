
var mongoose = require('mongoose');

var adminSchema = new mongoose.Schema({
	username: {
		type: String,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true
	}
});

var accountlist = mongoose.model('accountlist', adminSchema);

module.exports = { accountlist: accountlist };